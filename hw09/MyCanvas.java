/**
 * Mycanvas is a class of ASCII Art.
 *
 * @author Jonathan Makenene
 * @version P2
 *
*/

import java.util.*;
import java.io.*;

public class MyCanvas{
   private char [][] array;
   private int numRows;
   private int numCols;
   private char border;
   
   /**
    * load read my 2D arrays in the text file.
    *
    * @param datafile represent the data file.
    */
   public void load(String datafile)throws FileNotFoundException{
      Scanner fin = new Scanner(new FileInputStream(datafile));
      numRows = fin.nextInt();
      numCols = fin.nextInt();
      array = new char [numRows][numCols];
      fin.nextLine();
      int counter = 0;
      while(counter < numRows){
         char [] letters = fin.nextLine().toCharArray();
         array[counter] = letters;
         counter++;
      }
      fin.close();
   }
   
   
   /**
    * this method is displaying the image in the datafile.
    *
    */
   public void show(){
      int i,j;
      for(i = 0; i < array.length; i++){
         for(j = 0; j < array[i].length; j++){
            System.out.print(array[i][j]);
         }
         System.out.println();
      }
   }
   
   /**
    * Sets the color border of a char for the image.
    *
    * @param a holds my character.
    *
    */
   public void setBorder(char a){
      this.border = a;
   }
   
   /**
    * Designates a char color border to fill in the gaps.
    *
    * @param char color border
    * @param r number of Rows
    * @param c number of columns
    *
    */
   public void fill(char filler, int r, int c){
      Filler fill = new Filler(array,filler,border,r,c);
      fill.fillSpot();
   }

   /**
    *  space method is used to to separate the show method in the driver.
    */ 
   public void space(){
      System.out.println("================================================");
   }
}