/**
 * Demonstrates ASCII image filling algorithm.
 *
 * @author  Jonathan Makenene
 * @version P2
 */
import java.util.Scanner;
import java.io.*;
public class Driver
{
   public static void main(String [] args)throws FileNotFoundException
   {
      MyCanvas canvas= new MyCanvas();
   
      canvas.load("image1.txt");
      canvas.setBorder('1');
      canvas.show();
      canvas.space();
      canvas.fill('*',1,1);
      canvas.show();
   
   		// canvas.load("image2.txt");
   		// canvas.setBorder('*');
   		// canvas.show();
         //canvas.space();
   		// canvas.fill('~',3,10);
   		// canvas.fill('.',1,1);
   		// canvas.fill('.',1,15);
   		// canvas.fill('X',8,14);
   		// canvas.show();
   
   // 		canvas.load("image3.txt");
   // 		canvas.setBorder('M');
   // 		canvas.show();
            //canvas.space();
   // 		canvas.fill('~',27,50);
   // 		canvas.fill('.',56,62);
   // 		canvas.fill(' ',0,0);
   // 		canvas.show();
   // 
   // 		canvas.load("image4.txt");
   // 		canvas.setBorder('M');
   // 		canvas.show();
   //       canvas.space();
   // 		canvas.fill(' ',60,101);
   // 		canvas.fill(';',120,30);
   // 		canvas.fill(';',130,160);
   // 		canvas.show();
   }
}
