/**
 * Filler is a class that fill the empty spots on the ASCII art.
 *
 * @author Jonathan Makenene
 * @version P2
 *
*/
public class Filler{
   private Filler next;
   private char fill;
   private int r;
   private int c;
   private  char border;
   char[][] picture;
   
   /**
    * this class constructor.
    */
   public Filler(char[][] i, char a, char b, int newR, int newC){
      picture = i;
      fill = a;
      r = newR;
      c = newC;
      border = b;
   }
   
   /**
    * this method is used to fill in the gaps.
    *
   */
   public void fillSpot(){
      if(picture[r][c] == border){
         next = null;
      }else{
         picture[r][c] = fill;
         for (int i = r-1; i <= r+1; i++){
            for (int j = c-1; j <= c+1; j++){
               if (r == i && c == j || (i == r-1 && j != c) || (i == r+1 && j != c)){
                  next = null;
               }else if(!(i < 0 || j < 0 || i >= picture.length || j >= picture[i].length || picture[i][j] == border || picture[i][j] == fill)){
                  next = new Filler(picture,fill,border,i,j);
                  next.fillSpot();
               }
            }
         }
      }
   }
}