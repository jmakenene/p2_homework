/**
 * A simple class to impelment a movie.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */

import java.io.*;

public class Movie{

	/**
	* The current value of the Movie.
   */
	
   private String title; // current movie title
   private String genre; // current movie genre
   private  int year; // current movie year
	
   /**
   *  this constructor is assigning my attributes.
   */
   
   public Movie(String newTitle, String newGenre, int newYear){
      this.title = newTitle;//assigned title to new title
      this.genre = newGenre;//assigned genre to new genre
      this.year = newYear;//assigned year to new year
   }
   
   /**
   *  default constructor.
   */
   
   public Movie(){
      this.title = "Unknown";
      this.genre = "None";
      this.year = 0000;
   }
	
   @Override
   /**
   *Using the toString to print My
   *title
   *genre
   *year
   */
   
   public String toString(){
      return String.format("%-30s %-20s %4d\n", this.title, this.genre, this.year);
   }
   
   /**
   *@param PrintStream file is used to access my file.
   */
   
   public void writeToFile(PrintStream file){
      file.println(this.title);  //print title in the file
      file.println(this.genre);  //print genre in the file
      file.println(this.year);   //print year in the file
   }
   
   /**
   *@param newYear the value used to set year.
   */
   
   public void setYear(int newYear){
      this.year = newYear;
   }
   
   /**
   *@return year from the attributes.
   */
   
   public int getYear(){
      return year;
   }
   
   /**
   *@param title the value used to compare to my title attribute.
   *@return  title from the attributes and title in parameter if they match.
   */
   
   public boolean isMatchesTitle(String title){
      return(this.title.toLowerCase().contains(title));
   }
   
   /**
   *@param genre the value used to compare to my genre attribute.
   *@return  genre from the attributes and genre in parameter if they match.
   */
   
   public boolean isMatchesGenre(String genre){
      return(this.genre.equalsIgnoreCase(genre));
   }
}