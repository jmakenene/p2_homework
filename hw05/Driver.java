/**
 * A simple class to impelment a movie.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */
import java.io.*;

public class Driver{
   public static void main(String[] args) throws FileNotFoundException{
   
      MovieUI mui= new MovieUI("../data/tinymovielist.txt");
      mui.run();
   }
}