/**
* Personal Movie Inventory System.
*
* @author  Jonathan Makenene
* @version For P2
* 
* <pre>
* This is the starter code for the MovieUI version of the movie
* inventory system.
* </pre>
*/

import java.util.*;
import java.io.*;

public class MovieUI {
   private MovieContainer mc;
   private Scanner kb;
   
   public MovieUI(String datafile) throws FileNotFoundException{
      this.mc = new MovieContainer(datafile);
      this.kb = new Scanner(System.in);
   }
   
   /**
   *  execute the menuchoice method.
   *
   */
   
   public void run() throws FileNotFoundException{
      int choice;
      do {
         choice = menuChoice();
         if (choice == 1)
            enterMovie();
         else if (choice == 2)
            deleteMovie();
         else if (choice == 3)
            mc.display();
         else if (choice == 4)
            searchByGenre();
         else if (choice == 5)
            searchByTitle();
         else if (choice == 6)
            searchByYear();
      } while (choice != 0);
   
      mc.save();
   
      System.out.println("\nTHE END");
   }
   
   /**
	* Displays menu and get's user's selection
   *
	*@return the user's menu selection
	*/
   
   public int menuChoice(){
      int choice; // user's selection
   
      System.out.println("\n\n");
      System.out.print("------------------------------------\n");
      System.out.print("[1] Add a Movie\n");
      System.out.print("[2] Delete a Movie\n");
      System.out.print("[3] Search by Genre\n");
      System.out.print("[4] Search by Title\n");
      System.out.print("[5] Search by Year\n");
      System.out.print("---\n");
      System.out.print("[0] Exit Program\n");
      System.out.print("------------------------------------\n");
      do {
         System.out.print("Your choice: ");
         choice = kb.nextInt();
      } while (choice < 0 || choice > 6);
   
      return choice;
   }
   
   
   /**
	* Allow user to enter a new movie.
   *
	*/
    
   public void enterMovie() throws FileNotFoundException{
      String title;
      String genre;
      int year;
      
      kb = new Scanner(System.in);
      System.out.print("Enter movie title : ");
      title = kb.nextLine();
      System.out.print("Enter movie genre : ");
      genre = kb.nextLine();
      System.out.print("Enter movie year : ");
      year = kb.nextInt();
      
      kb.nextLine();
      
      mc.insert(title,genre,year);
   }
   
   /**
	* Deleting movie from the DATAFILE.
	*/
    
   public void deleteMovie(){
      int delMovie;
      searchByTitle();
      System.out.print("Enter the number next to the movie you wish to get rid of: ");
      delMovie = kb.nextInt();
      kb.nextLine();
      mc.remove(delMovie);
   }
   
   /**
   * Searching Movie by Entering the Title.
   */
   
   public void searchByTitle(){
      kb.nextLine();
      String title;
      System.out.print("Enter title: "); // get title from user
      title = kb.nextLine();
      mc.searchByTitle(title);
   }
   
   /**
   * Searching Movie by Entering the Genre.
   */
   
   public void searchByGenre(){
      kb.nextLine();
      String genre;
      System.out.print("Enter genre: "); // get genre from user
      genre = kb.nextLine();
      mc.searchByGenre(genre);
   }
   
   /**
   * Searching Movie by Entering the year.
   */
   
   public void searchByYear(){
      int year;
      System.out.print("Enter year: ");   // get year from user
      year = kb.nextInt();
      kb.nextLine();
      mc.searchByYear(year);
   }
}
