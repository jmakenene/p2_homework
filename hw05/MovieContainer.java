/**
* Personal Movie Inventory System.
*
* @author  Jonathan Makenene
* @version For P2
* 
* <pre>
* This is the starter code for the Movie Container version of the movie
* inventory system.
* </pre>
*/

import java.util.*;
import java.io.*;

public class MovieContainer {
   private Movie[] movies; //current Array of object
   private int numMovies; 
   private String datafile;// current movie file
   private final int MAXMOVIES = 10000;// total of movie
   
   
   /**
   *
   *The Constructor of the class
   *
   *@param access the DATAFILE
   */
   
   public MovieContainer(String datafile) throws FileNotFoundException{
      this.datafile = datafile;
      numMovies = 0;
      movies = new Movie[MAXMOVIES];
         
      Scanner fin = new Scanner(new FileInputStream(this.datafile));
      
      String title,genre;
      int year;
         
      while(fin.hasNextLine()){
         //movies[numMovies] = new Movie(title,genre,year);
         // movies[numMovies].title=fin.nextLine();
//          movies[numMovies].genre=fin.nextLine();
//          movies[numMovies].year = fin.nextInt();
         
          title = fin.nextLine();
          genre = fin.nextLine();
          year = fin.nextInt();   
          fin.nextLine();
            
      }
         
   }
	
   /**
   * Allow to insert movies.
   *
   *@param title to insert title which is a String.
   *@param genre to insert genre which is a String.
   *@param year to insert year which is a int.
   *
   */
   
   public void insert(String title,String genre,int year) throws FileNotFoundException {
      //this purpose of this method is to add movies inside my array.
      movies[numMovies] = new Movie(title,genre,year);
      movies[numMovies].isMatchesTitle(title);
      movies[numMovies].isMatchesGenre(genre);
      movies[numMovies].setYear(year);
      numMovies++;
      
      save();
   }
   
   /**
   * remove entry from container
   *
   *@param index keeping track of my index in the array.
   */
   
   public void remove(int index){
      //I'm using this method to remove movies that i don't want in the Movie Array.
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      System.out.print(movies[index]);
      movies[index] = movies[numMovies - 1];
      numMovies--;
   }
   
   /**
   * save container contents to data file
   *
   */
   
   public void save()throws FileNotFoundException {
      PrintStream file = new PrintStream(this.datafile); // using the PrintStream class to access my DATAFILE and then save each movie that gets printed in the DATAFILE.
      for(int i = 0; i < numMovies; i++){
         movies[i].writeToFile(file);
      }
      file.close();
   }
   
   /**
   * Displays all movie information from DATAFILE.
   *
   */
   
   public void display(){
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for (i = 0; i < numMovies; i++){
         System.out.print("["+i+"]");
         System.out.print(movies[i]);
      }
   }
   
   /**
   * Size returns number of entries in container.
   *
   */
   
   public int size(){
      return numMovies;
   }
   
   /**
   * Searching Movie by Title.
   *
   *@param title find the movie wanted.
   */
   
   public void searchByTitle(String title){
      // the title of the movie you want.
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(i = 0; i < numMovies; i++){
         if(movies[i].isMatchesTitle(title)){   //calling the method isMatchesTitle to check if my movie matches.
            System.out.print("["+i+"]");
            System.out.print(movies[i]);
         }
      }
   }
   
   /**
   * Searching Movie by Genre
   *
   *@param genre type of movie.
   */
   
   public void searchByGenre(String genre){
      // genre what type of movies is it?
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(i = 0; i < numMovies; i++){
         if(movies[i].isMatchesGenre(genre)){ //calling the method isMatchesGenre to check if my genre matches.
            System.out.print(movies[i]);
         }
      }
   }
   
   /**
   * Searching Movie by Year.
   *
   *@param year Movie release data.
   */
   
   public void searchByYear(int year){
      //What year the movie came out?
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(i = 0; i < numMovies; i++){
         if(movies[i].getYear() == year){ // checking if my year is equal to the year in the Movie array.
            System.out.print(movies[i]);
         } 
      }
   }
}
