/**
 * A simple class to impelment a movie.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */


public class Movie{

	/**
	 * The current value of the Movie.
	 */
	
	public String title; // current movie title
	public String genre; // current movie genre
	public int year; // current movie year
	

	public Movie(String newTitle, String newGenre, int newYear){
		this.title = newTitle;//assigned title to new title
		this.genre = newGenre;//assigned genre to new genre
		this.year = newYear;//assigned year to new year
	}	
	

    @Override
	/**
	*Using the toString to print My
	*title
	*genre
	*year
	*/
	public String toString(){
		return String.format("%-30s %-20s %4d\n", this.title, this.genre, this.year);
	}
	
}