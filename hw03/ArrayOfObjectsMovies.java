/**
* Personal Movie Inventory System.
*
* @author  Jonathan Makenene
* @version For P2
* 
* <pre>
* This is the starter code for the Array Of Objects version of the movie
* inventory system.
* </pre>
*/

import java.util.*;
import java.io.*;

public class ArrayOfObjectsMovies {
//public static final String DATAFILE = "../data/movielist.txt";
public static final String DATAFILE = "C:/Users/jonat\\p2_homework\\data\\movielist.txt";
public static final int MAXMOVIES = 10000;

	public static void main(String[] args) throws FileNotFoundException {
		int choice; // user's selection from the menu

		Movie[] movies = new Movie[MAXMOVIES];//Array of type Movie that contains movie,genre,and year,
		int numberOfEntries;


		numberOfEntries = loadMovies(movies);
		System.out.println("Number of entries read from data file: " + numberOfEntries);
		do {
			choice = getMenuChoice();
			if (choice == 1)
				numberOfEntries = enterMovie(movies, numberOfEntries);
			else if (choice == 2)
				numberOfEntries = deleteMovie(movies, numberOfEntries);
			else if (choice == 3)
				displayAll(movies, numberOfEntries);
			else if (choice == 4)
				searchByGenre(movies, numberOfEntries);
			else if (choice == 5)
				searchByTitle(movies, numberOfEntries);
			else if (choice == 6)
				searchByYear(movies, numberOfEntries);
			} while (choice != 0);

		saveMovie(movies, numberOfEntries);

		System.out.println("\nTHE END");
	}

	/**
	 * Allow user to enter a new movie.
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @param n      the actual number of movies currently in the array
	 * @return the new movie count
	 */
	public static int enterMovie(Movie[] movies, int n) {
		String title;
		String genre;
		int year;

		Scanner kb = new Scanner(System.in);
		System.out.print("Enter movie title : ");
		title = kb.nextLine();
		System.out.print("Enter movie genre: ");
		genre = kb.nextLine();
		System.out.print("Enter year of movie: ");
		year = kb.nextInt();
		kb.nextLine();
	
		movies[n] = new Movie(title, genre, year);
	
		return n + 1;
	}
	
	/**
	 * Load movies from the data file into the arrays
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @return the actual number of movies loaded into the arrays
	 */
	public static int loadMovies(Movie[] movies) throws FileNotFoundException{
	
	// i"m passing in three variables to the Movie constructor, two strings and an int. 
		String titles;
		String genres;
		int years;

		Scanner movieFile = new Scanner(new FileInputStream(DATAFILE));
		int num = 0; //using num to keep track of my index 

		while(movieFile.hasNextLine()){ 
	
			titles = movieFile.nextLine();
			genres = movieFile.nextLine();
			years = movieFile.nextInt();
			movieFile.nextLine(); // to move to the next line after reading the int
			//now that i have assigned the title, genre and year to each variable, pass them to the constructor to put them in the movies [] array at the num index
			movies[num] = new Movie(titles, genres, years);
			num++;
		}
		movieFile.close();
		return num;
	}
	
	/**
	 * Deleting movie from an array.
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @param n      the actual number of movies currently in the array
	 */
	
	public static int deleteMovie(Movie[] movies, int n) {

		Scanner kb = new Scanner(System.in);
		int arrayDel = 0;
		searchByTitle(movies, n);
		System.out.print("Enter the number next to the movie you wish to get rid of: ");
		arrayDel = kb.nextInt();
		kb.nextLine();
		System.out.println("------------------------------------------------");
		System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");

			if(arrayDel >= 0 && arrayDel < n){
				for(int i = arrayDel; i < n-1; i++){
					System.out.print(movies[i]);
						//movies[i].title = movies[i + 1].title;
						//movies[i].genre = movies[i + 1].genre;
						//movies[i].year = movies[i + 1].year;
						movies[i] = movies[i+1];
				}	
			}else{
		return n;
			}
		return n-1;
	}

	/**
	 * Displays all movie information.
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @param n      the actual number of movies currently in the array
	 */
	public static void displayAll(Movie[] movies, int n) {
		int i;
		System.out.println("------------------------------------------------");
		System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
		for (i = 0; i < n; i++)
			System.out.print(movies[i]);
	}

	/**
	 * Displays menu and get's user's selection
	 *
	 * @return the user's menu selection
	 */
	public static int getMenuChoice() {
		Scanner kb = new Scanner(System.in);
		int choice; // user's selection

		System.out.println("\n\n");
		System.out.print("------------------------------------\n");
		System.out.print("[1] Add a Movie\n");
		System.out.print("[2] Delete a Movie\n");
		System.out.print("[3] List All Movies\n");
		System.out.print("[4] Search by Genre\n");
		System.out.print("[5] Search by Title\n");
		System.out.print("[6] Search by Year\n");
		System.out.print("---\n");
		System.out.print("[0] Exit Program\n");
		System.out.print("------------------------------------\n");
		do {
			System.out.print("Your choice: ");
			choice = kb.nextInt();
		} while (choice < 0 || choice > 6);

		return choice;
	}

	
	/**
	 * Searching movies by entering the genre.
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @param n      the actual number of movies currently in the array
	 */
	
	public static void searchByGenre(Movie[] movies, int n){
	
		Scanner kb = new Scanner(System.in);
		System.out.print("Search by Genre: ");
		String genre = kb.nextLine();
		int i = 0;
		System.out.println("------------------------------------------------");
		System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
			for( i = 0; i < n; i++){
				if(genre.equalsIgnoreCase(movies[i].genre)){
					System.out.print(movies[i]);
				}
			}
	}
	
	/**
	 * Searching movie by titles.
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @param n      the actual number of movies currently in the array
	 */
	public static void searchByTitle(Movie[] movies, int n) {
		int i;
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter the Title: ");
		String title = kb.nextLine();
		System.out.println("------------------------------------------------");
		System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
			for (i = 0; i < n; i++){
				if(movies[i].title.toLowerCase().contains(title)){
					System.out.print("["+i+"]");
					System.out.print(movies[i]);
				}
			}
	}
	
	/**
	 * Searching movies by year(Release Date)
	 * 
	 * @param movies array of movie that contain titles genres and years
	 * @param n  the actual number of movies currently in the array
	 */

	public static void searchByYear(Movie[] movies, int n) {

		Scanner kb = new Scanner(System.in);
		System.out.print("Search by Year: ");
		int year = kb.nextInt();
		int i;
	
		System.out.println("------------------------------------------------");
		System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");

		for (i = 0; i < n; i++) {
			if (year == movies[i].year) {
				System.out.print(movies[i]);
			}
		}
	}
	
	/**
	 * saveMovie method is used to access and print in the file .
	 *
	 * @param movies array of movie that contain titles genres and years
	 * @param n      the actual number of movies currently in the array
	 */
	 
	public static void saveMovie(Movie[] movies, int n) throws FileNotFoundException{
		PrintStream f = new PrintStream(DATAFILE);
			for(int i = 0; i < n; i++){
				f.println(movies[i].title);
				f.println(movies[i].genre);
				f.println(movies[i].year);
			}

			f.close();
	}
}
