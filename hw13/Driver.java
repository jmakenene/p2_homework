/**
 * Driver for testing LinkedDictionary code.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

public class Driver
{
   public static void main(String[] args) throws Exception
   {  
      CodeTimer timer = new CodeTimer();
      timer.start(); 
      ArrayDictionary a = new ArrayDictionary("word.txt");      
      a.displayBigAnagramFamilies();
      timer.stop();
      System.out.println("timer:"+timer);
   }
}

