/**
 * Provides LinkedDictionary class that's inherit from the WordList class.
 *
 * @author Jonathan Makenene
 * @version for Program Design 2
 *
 *
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Collections;

public class ArrayDictionary extends ArrayList<Word>{//inheriting from the arraylist
  
   /**
    * the class constructor.
    *
    * @param datafile read from the file
    */   
   public ArrayDictionary(String datafile)throws IOException{
      super();
      BufferedReader br;
      br = new BufferedReader(new FileReader(datafile));
      String line;   
      while((line = br.readLine())!= null){
         this.add(new Word(line));
      }
      Collections.sort(this);
   }
   
   /**
    * in this I'm using the ListIterator and then using loop to go trough it and then display it.
    *
    */
   public void display(){
      ListIterator<Word> it;
      it = listIterator();
      
      while(it.hasNext()){
         System.out.println(it.next());
      }
   }
      
   /**
    * this method keep count of the anagrams. 
    *
    * @param w the value to be counted  
    * @return the number of anagrams
    */
   public int countAnagrams(Word w){
      int index = Collections.binarySearch(this,w);
      int counter = 0;
      
      if(index == -1) 
         return -1;
      
      for(int i=index; i<this.size() && get(i).compareTo(w) == 0; i++){
         counter++;
      }         
   
      for(int j=index; j<this.size() && get(j).compareTo(w) > 0; j--){
         counter++;
      }
      return counter;
   }
   
   /**
    * this method I'm using a list iterator to go through my arraylist and display all the anagrams.
    *
    *
    */
   public void displayBigAnagramFamilies(){     
      ListIterator<Word> it;
      it = listIterator();
      
      while(it.hasNext()){
         int count = countAnagrams(it.next());
         if(count >= 6){
            System.out.println(it.next());
         }
      }
   }
   
}