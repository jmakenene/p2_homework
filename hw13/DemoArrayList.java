/**
 * Demonstrates use of Java's ArrayList.
 *
 * @author  TSergeant
 * @version for Program Design 2
 */

import java.util.ArrayList;

public class DemoArrayList
{
   public static void main(String [] args)
   {
      /*ArrayList<String> al;
      ArrayList<Integer> il;
      al= new ArrayList<String>();
      il = new ArrayList<Integer>();
      
      al.add("hi");
      al.add("there");
      al.add("Jonathan");
      il.add(21);
      System.out.println(al.size()+"   "+il.size());
   	//System.out.println(al.get(0));
      //System.out.println(al.get(1));
      System.out.println(al.indexOf("there"));
      System.out.println(al.indexOf("Jonathan"));
      System.out.println(il.indexOf(21));*/
      
      ArrayList<Word> a;
      a = new ArrayList<Word>();
      a.add(new Word("oocl"));
      a.add(new Word("cool"));
      a.add(new Word("silent"));
      a.add(new Word("listen"));
      
      System.out.println("size:"+a.size());      
      System.out.println(a.get(0).equals(a.get(1)));
      System.out.println(a.get(2).equals(a.get(3)));
      System.out.println(a.get(2).hashCode());
      System.out.println(a.get(1).hashCode());
      System.out.println(a.get(2).isAnagram(a.get(3)));
      System.out.println(a.get(0).isAnagram(a.get(1)));
      
      int result = a.get(0).compareTo(a.get(0));
      System.out.println("result:"+result);
   }
}
