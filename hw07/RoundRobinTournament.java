/**
 * A Round Robin tournament class.
 *
 * @author  Jonathan Makenene
 * @version for P2
 *
*/

public class RoundRobinTournament extends RPSTournament{
   
   /**
    * This method is used to play a Rock,paper,scissors game on round robin tournament between players.
    *
    */
   public void play(){      
      System.out.println("Welcome to the game of Rock,Paper,Scissors");
      for(int i = 0; i < numPlayers-1; i++){
         for(int j = i+1; j < numPlayers; j++){
            players[i].fight(players[j]);
         } 
      }      
   }
}