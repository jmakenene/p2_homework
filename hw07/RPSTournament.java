/**
 * A RPS tournament class.
 *
 * @author  Jonathan Makenene
 * @version for P2
 *
*/

public abstract class RPSTournament extends RPSPlayerContainer{
   
   /**
    * The Constructor of the class
    */
   public RPSTournament(){
      super();
   }
    
   /**
    * this method is sorting my array of players.
    *
    */ 
   public void sort(){
      int i,j;
      for(i = 0; i < numPlayers-1; i++){
         for(j = i+1; j < numPlayers; j++){
            if(players[i].compareTo(players[j]) < 0){
               RPSPlayer temp = players[i];
               players[i] = players[j];
               players[j] = temp;
            }
         }
      }
   }
   
   /**
    * modified my play method to abstract.
    */
   public abstract void play();//it's abstract so therefore this method does not contain a body.
}