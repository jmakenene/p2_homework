/**
 * A player container of Rock, Paper, Scissors player.
 * 
 *
 * @author  Jonathan Makenene
 * @version for P2
 *
*/

public class RPSPlayerContainer {
   protected RPSPlayer [] players; //an array of players
   protected int numPlayers; //the number of players in the container
   
   public RPSPlayerContainer(){
      players = new RPSPlayer[100]; //reserves 100 slots in the array
      numPlayers = 0;
   }
   
   /**
    *  this method adds players to the container.
    *
    *  @param p the number of new players.
    */
   public void add(RPSPlayer p){
      players[numPlayers] = p;
      numPlayers++;
   }
   
   /**
    * this method displays all the player in the container.
    *
    */
   public void display(){
      int i;
      for(i = 0; i < numPlayers; i++){
        players[i].display();
      }
   }
   
   /**
    * Size returns number of players in container.
    */
   public int size(){
      return numPlayers;
   }
}