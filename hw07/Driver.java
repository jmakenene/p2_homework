/**
 * A Driver class to test out my code.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */

import java.util.Scanner;

public class Driver
{
   public static void main(String [] args)
   {  
      ComputerRPSPlayer a,b,c,d,e,f;
      a= new ComputerRPSPlayer("a");
      b= new ComputerRPSPlayer("b");
      c= new ComputerRPSPlayer("c");
      d= new ComputerRPSPlayer("d");
      e= new ComputerRPSPlayer("e");
      f= new ComputerRPSPlayer("f");
      RoundRobinTournament rt = new RoundRobinTournament();
      //adding players in the tournament  
      rt.add(a);
      rt.add(b);
      rt.add(c);
      rt.add(d);
      rt.add(e);
      rt.add(f);
      rt.play();
      //sorting the players in the array.      
      rt.sort();
      //displaying my new players
      rt.display();
   }
}

