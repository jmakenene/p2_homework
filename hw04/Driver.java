/**
 * A simple class to impelment a movie.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */
import java.io.*;

public class Driver{
   public static void main(String[] args) throws FileNotFoundException{
   
      MovieContainer movieContainer = new MovieContainer("../data/tinymovielist.txt");
      movieContainer.insert("The Amazing Spider-Man","Action",2012);
      movieContainer.insert("Attack On Titan","Anime",2013);
      movieContainer.insert("DeadPool","Action",2016);
      movieContainer.insert("Tomb Raider","Action",2018);
      movieContainer.insert("Naruto","Anime",2002);
      movieContainer.insert("Power","Action",2014);
      //movieContainer.searchByTitle("r");
      //movieContainer.searchByGenre("anime");
      //movieContainer.searchByYear(2002);
      //movieContainer.remove(1);
      //movieContainer.remove(0);
      //movieContainer.remove(2);
      //movieContainer.remove(5);
      movieContainer.display();
      movieContainer.save();
      movieContainer.size();
   }
}