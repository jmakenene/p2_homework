/**
 * A clicker is used to count things.
 *
 * @author	Jonathan Makenene
 * @version	P2
 *
*/

public class Clicker
{	
	/**
	*These are my attributes
	*/
   private int counter;//keeping count of my clicker
   private int countmax;
	
	
   Clicker(){
      counter = 0;
      countmax = 9999;//setting my clicker's to the max 9999;
   }
	/**
	*using this method to get my private attribute
	*/
   public int getCounter(){
      return counter;
   }
	/**
	*checking my counter and my countmax
	*/
	public void add(){
      if(counter < countmax){
         counter++;
      }else{
			reset();
		}
   }
	/**
	*method to reset my clicker
	*/
	public void reset(){
      counter = 0;
   }
	
	@Override
	/**
	*Overriding my toString
	*/
	public String toString(){
      return ""+counter;
   }
	
}

