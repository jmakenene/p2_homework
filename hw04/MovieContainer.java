/**
* Personal Movie Inventory System.
*
* @author  Jonathan Makenene
* @version For P2
* 
* <pre>
* This is the starter code for the Movie Container version of the movie
* inventory system.
* </pre>
*/

import java.util.*;
import java.io.*;

public class MovieContainer {
   private Movie[] movies; //current Array of object
   private int numMovies; 
   private String DATAFILE;// current movie file
   private final int MAXMOVIES = 10000;// total of movie
   
   
   /**
   *
   *The Constructor of the class
   *
   *@param access the DATAFILE
   */
   public MovieContainer(String DATAFILE) throws FileNotFoundException{
      this.DATAFILE = DATAFILE;
      numMovies = 0;
      movies = new Movie[MAXMOVIES];
         
      Scanner fin = new Scanner(new FileInputStream(this.DATAFILE));
         
      while(fin.hasNextLine()){
         movies[numMovies] = new Movie();
         movies[numMovies].title = fin.nextLine();
         movies[numMovies].genre = fin.nextLine();
         movies[numMovies].year = fin.nextInt();
            
         fin.nextLine();
            
      }
         
   }
	
   /**
   * Allow to insert movies.
   *
   *@param title to insert title which is a String.
   *@param genre to insert genre which is a String.
   *@param year to insert year which is a int.
   *
   */
   
   public void insert(String title,String genre,int year) throws FileNotFoundException {
      movies[numMovies] = new Movie();
      movies[numMovies].title = title;
      movies[numMovies].genre = genre;
      movies[numMovies].year = year;
      numMovies++;
      
      save();
   }
   
   /**
   * remove entry from container
   *
   *@param index keeping track of my index in the array.
   */
   
   public void remove(int index){
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      System.out.print("["+index+"]");
      System.out.print(movies[index]);
      movies[index] = movies[numMovies - 1];
      numMovies--;
   }
   
   /**
   * save container contents to data file
   *
   */
   
   public void save()throws FileNotFoundException {
      PrintStream fin = new PrintStream(this.DATAFILE); // using the PrintStream class to access my DATAFILE and then save each movie that gets printed in the DATAFILE.
      int i;
      for(i = 0; i < numMovies; i++){
         fin.println(movies[i].title);
         fin.println(movies[i].genre);
         fin.println(movies[i].year);
      }
      fin.close();
   }
   
   /**
   * Displays all movie information from DATAFILE.
   *
   */
   public void display(){
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for (i = 0; i < numMovies; i++){
         System.out.print("["+i+"]");
         System.out.print(movies[i]);
      }
   }
   
   /**
   * Size returns number of entries in container.
   *
   */
   
   public int size(){
      return numMovies;
   }
   
   /**
   * Searching Movie by Title.
   *
   *@param title find the movie wanted.
   */
   
   public void searchByTitle(String title){
      // the title of the movie you want.
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(i = 0; i < numMovies; i++){
         if(movies[i].title.toLowerCase().contains(title)){ // checking if my title is equal to the title in the Movie.

            System.out.print("["+i+"]");
            System.out.print(movies[i]);
         }
      }
   }
   
   /**
   * Searching Movie by  Genre
   *
   *@param genre type of movie.
   */
   
   public void searchByGenre(String genre){
      // genre what type of movies is it?
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(i = 0; i < numMovies; i++){
         if(genre.equalsIgnoreCase(movies[i].genre)){ // checking if my genre is equal to the genre in the Movie array and also making it case insensitive.
            System.out.print(movies[i]);
         }
      }
   }
   
   /**
   * Searching Movie by Year.
   *
   *@param year Movie release data.
   */
   
   public void searchByYear(int year){
      //What year the movie came out?
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(i = 0; i < numMovies; i++){
         if(movies[i].year == year){ // checking if my year is equal to the year in the Movie array.
            System.out.print(movies[i]);
         } 
      }
   }
}