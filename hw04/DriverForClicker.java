/**
 * This driver is for testing the Clicker class.
 *
 * @author	Jonathan Makenene
 * @version	P2
 *
*/
import java.util.Scanner;

public class DriverForClicker
{
   public static void main(String [] args)
   {
      Clicker one,two,three;
   	   
   	System.out.println("Welcome to the Clicker tester");
   	
      Scanner kb = new Scanner(System.in);
   	
      System.out.print("Enter how many times clicker one should count:");
      int c1 = kb.nextInt();
		//random number to test my clicker one
      System.out.print("Enter how many times clicker two should count:");
      int c2 = kb.nextInt();
		//random number to test my clicker two
      System.out.print("Enter how many times clicker three should count:");
      int c3 = kb.nextInt();
		//random number to test my clicker three
   	
   	
      one = new Clicker();
      two = new Clicker();
      three = new Clicker();
   	
   	System.out.print("\n\n click one: ");
      for(int i=0; i<c1; i++){
         one.add();
         System.out.print(one+" ");
      }
   	
		System.out.print("\n\n click two: ");
      for(int i=0; i<c2; i++){
         two.add();
         System.out.print(two+" ");
      }
   	
		
		System.out.print("\n\n click three: ");
      for(int i=0; i<c3; i++){
         three.add();
         System.out.print(three+" ");
      }
   }
}

