/**
 * A Driver class to test out my code.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */

import java.util.Scanner;

public class Driver
{
   public static void main(String [] args)
   {
      ComputerRPSPlayer a;
      HumanRPSPlayer b;
      a= new ComputerRPSPlayer("George");
      b= new HumanRPSPlayer("Jonathan");
      
      RPSPlayerContainer rc = new RPSPlayerContainer();
   
      
      a.display();
      b.display();
      
      rc.add(a);
      rc.add(b);
      
      // my loop will keep running until one of the players gets three wins.
      while(a.getWins() < 3 && b.getWins() < 3){
         a.fight(b);
      }
      
      a.display();
      b.display();
   }
}

