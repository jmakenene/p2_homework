/**
 * A linked list of integer values.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
public class IntList
{
   private IntNode head;

	/**
	 * A new list has head pointing nowhere.
	 */
   public IntList()
   {
      head= null;
   }


	/**
	 * Displays contents of the list.
	 */
   public void display()
   {
      IntNode p;
      for (p = head; p != null; p = p.next) {
         System.out.println(p.data);
      }
   }



	/**
	 * In an unordered list we can just add to the front.
	 *
	 * @param newdata The new element to be inserted into the list.
	 */
   public void insert(int newdata)
   {
      IntNode p;
      p = new IntNode();
      p.data = newdata;
      p.next = head;
      head = p;
   }


	/**
	 * Search the list for the value val.
	 *
	 * @param val the value to be searched for
	 * @return reference to the found node (null if not found)
	 */
   public IntNode search(int val)
   {
      IntNode p;
      for(p = head; p != null; p = p.next){
         if(p.data == val){
            return p;
         }
      }
      return null;
   }


	/**
	 * Find first occurrence of val (if it exists) and remove it from the list.
	 *
	 * @param val the value to be removed
	 */
   public void remove(int val)
   {
      IntNode p,q;
      p = head;
      while(p != null && p.data == val){
         p = p.next;
      }
      
      q = p;
      while(p != null && p.next != null){
         if(p.next.data == val){
            p.next = p.next.next;
         }else{
            p = p.next;
         }
      }
   }
}
