/**
 * Driver for testing WordList code.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

public class Driver
{
   public static void main(String [] args) throws IOException
   {        
      WordList list = new WordList();
      BufferedReader br;
      br = new BufferedReader(new FileReader("words.txt"));
      Word w;
      String line = br.readLine();      
      while(line != null){
         //System.out.println(line);
         w = new Word(line);
         list.insert(w);
         line = br.readLine();
      }
      br.close();
      list.display();
      
      /*IntNode p;
      IntList list = new IntList();
      for(int i = 1; i <= 5; i++){ 
         list.insert(i);
      }
      
      p = list.search(0);
      if(p == null){
         System.out.println("The Number was not found");
      }else{
         System.out.println("The Number was found");
         
      }
      
      list.remove(3);
      list.remove(2);
      list.remove(4);
      
      list.display();*/
      
      /*Word w = new Word("Jonathan");
      System.out.println(w);*/
      
      /*WordNode word = new WordNode();
      
      word.data = null;
      word.next = null;*/
      
      /*WordList list = new WordList();
      Word v,w;
      v = new Word("jonathan");
      w = new Word("Mathias");
      list.insert(v); 
      list.insert(w);
      list.display(); */
   }
}

