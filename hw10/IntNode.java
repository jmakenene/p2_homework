/**
 * A linked list node holding integer data.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
public class IntNode
{
   int data;
   IntNode next;
   
}

