/**
 * Provides LinkedDictionary class that's inherit from the WordList class.
 *
 * @author Jonathan Makenene
 * @version for Program Design 2
 *
 *
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ArrayDictionary{
   
   private Word[] data;
   private int n;
   /**
    * the class constructor.
    *
    * @param datafile read from the file
    */   
   public ArrayDictionary(String datafile)throws IOException{
      this.data = new Word[500000];
      this.n = 0;
      BufferedReader br;
      br = new BufferedReader(new FileReader(datafile));
      String line;    
      while((line = br.readLine())!= null){
         this.data[n++] = new Word(line);
      }
      sort();
   }
   
   public void display(){
      for(int i = 0; i<this.n; i++){
         System.out.println(this.data[i]);
      } 
   }
      
   /**
    * this method keep count of the anagrams. 
    *
    * @param w the value to be counted  
    * @return the number of anagrams
    */
   public int countAnagrams(Word w){
      int result = binSearch(w,0,this.n-1);
      int counter = 0;
      if(result != -1){
         counter++;
         int sol = result;
         
         do{
            if(sol == this.n-1){
               break;
            }
            sol++;
            if(this.data[sol].compareTo(w)==0){
               counter++;
            }
         }while(this.data[sol].compareTo(w)==0);
         
         sol = result;
         
         do{
            if(sol == 0){
               break;
            }
            sol--;
            if(this.data[sol].compareTo(w)==0){
               counter++;
            }
         }while(this.data[sol].compareTo(w)==0);
      }
      return counter;
   }
   
   /**
    * this method is checking for all the anagrams.
    *
    *
    */
   public void displayBigAnagramFamilies(){
      for(int i = 0; i < this.n; i++){
            int count = countAnagrams(this.data[i]);
            if(count >= 6){
                System.out.println(data[i]);
            }
            i+=count-1;
        }
   }
   
   /**
    * sort is calling the quickSort method to sort the array.
    */
   public void sort(){
      qSort(this.data,0,this.n-1);
   }
   
   /**
    * this method take the last element as k and places the k in the correct position.
    *
    * @param p serves as my first element
    * @param r serves as my last element
    * @return an int
    */
   private int partition(Word[] a,int p, int r){
      Word k = a[p];
      Word temp;
      int i = p - 1;
      int j = r + 1;
      
      do{
         do{j--;}while(a[j].compareTo(k) > 0);
         do{i++;}while(a[i].compareTo(k) < 0);
         if(i<j){
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
         }
      }while(i<j);
      return j; 
   }
   
   /**
    * the quickSort method is calling the partition method sorting the array.
    *
    * @param p serves as my first element
    * @param r serves as my last element
    */
   private void qSort(Word[] array, int p, int r){
      if(!(p >= r)){
         int newPos = partition(array, p, r);
         qSort(array, p, newPos);
         qSort(array, newPos + 1, r);
      }
   }
   
   /**
    * the private binary search is search for searching for anagrams word in the array.
    *
    * @param start starts at the beginning of the array
    * @param stop stops at the end of the array
    * @param w is type Word is being used to compare
    * @return int
    */
   public int binSearch(Word w, int start, int stop){
      if(start > stop){
         return -1;
      }else{
         int pos = (start + stop) / 2;
         if(w.compareTo(this.data[pos]) == 0){
            return pos;
         }else{
            if(w.compareTo(this.data[pos]) < 0){
               return binSearch(w, start, pos-1);
            }else{
               return binSearch(w, pos+1, stop);
            }
         }
      }
   }
   
   /**
    * this method is calling the binSearch method to check the array.
    * @param w is being passed in as a parameter
    * @return int
    */
   // public int binSearch(Word w){
//       return binSearch(0,this.n-1,w);
//    }
}