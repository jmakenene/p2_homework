/**
 * Driver for testing LinkedDictionary code.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Driver
{
   public static void main(String[] args) throws IOException
   {  
       
      ArrayDictionary a = new ArrayDictionary("word.txt");
      /*int result = a.binSearch(new Word("cool"));
      int solution = a.countAnagrams(new Word("oclo"));
      System.out.println("result:"+result);
      System.out.println("solution:"+solution);
      a.display();*///../data/mywords.txt
      //System.out.println(w);*/
      CodeTimer timer = new CodeTimer();
      timer.start();      
      a.displayBigAnagramFamilies();
      timer.stop();
      System.out.println("timer:"+timer);      
   }
}

