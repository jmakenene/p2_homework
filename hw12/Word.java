/**
 * Provides a simple word object that stores a word together with its letters
 * sorted.
 *
 * @author Jonathan Makenene
 * @version for Program Design 2
 *
 *
 */
import java.util.Arrays;

public class Word
{
   private String word;    // the word
   private String sorted;  // the word re-arranged so that its letters are sorted

	/**
	 * Store the word and create a sorted version of the word.
	 *
	 * @param word the word to be stored
	 */
   public Word(String word)
   {
      char [] letters= word.toCharArray();
      Arrays.sort(letters);
      this.word= word;
      this.sorted= new String(letters);
   }

	/**
	 * Just show the regular word when object is printed.
	 *
	 * @return the regular word
	 */
   public String toString()
   {
      return this.word + "(" + sorted + ")";
   }
   
   
   /**
    * the purpose of this equals method is to basically Override the built-in equals method.
    *
    * @ param obj is a variable of type object
    * @ return boolean true || false.
    */
   @Override
   public boolean equals(Object obj){
      if(!(obj instanceof Word)) {
         return false;
      }   
      if(this == obj) {
         return true;
      }
      Word temp = (Word) obj;
      return this.word.equals(temp.word);
   }
   
   /**
    * hashCode method purpose is to retrun a hashcode value of the object on calling.
    * 
    * @return an Integer
    */
   @Override
   public int hashCode(){
      return word.hashCode();
   }
   
   /**
    * this method checks if the sorted words are anagrams or not.
    *
    * @param w this value holds the sorted word
    */
   public boolean isAnagram(Word w){
      if(this.sorted.equals(w.sorted)){
         return true;
      }else{
         return false;
      }   
   }
   
   /**
    * this method compare two objects.
    *
    * @param w holds a sorted word
    * @return an int
    */
   public int compareTo(Word w){
      int result = this.sorted.compareTo(w.sorted);
      return result;
   }
}
