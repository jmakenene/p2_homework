/**
 * A Palindrome class to check if the words are palindrome or not.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */
 
import java.util.*;
import java.io.*;

public class WordContainer
{
   String datafile;
   String [] words;
   int countWord,countPalin;
   BufferedReader br;
   
   /**
    * This class Constructor.
    *
    * @param datafile this variable holds my text file.
    */
   public WordContainer(String datafile) throws FileNotFoundException{
      this.datafile = datafile; 
      br = new BufferedReader(new FileReader(this.datafile));
      words = new String[10000];  
      countWord = 0;
      countPalin = 0;      
   }
   
   /**
    * this method is checking to how much words i have in my file and then checks how many of them are palindrome.
    *
    */
   public void checkPalindrome()throws IOException{
      String word = "";
      do{
         word = br.readLine();
         if(word != null){         
            words[countWord] = word;
            countWord++;        
            if(isPalindrome(word)){ //checking to see how many words in the file are palindrome.
               countPalin++;
            }
         }
      }while(word != null);
      
   }
   
   /**
    * this method is printing out all my result.
    *
    */
   public void getResults(){
      double percentage = (double) countPalin / countWord* 100;
      System.out.println("Input File      : " + this.datafile);
      System.out.println("Words Processed : "+countWord);
      System.out.println("# Palindromes   : "+countPalin);
      System.out.printf("%% Palindromes   : %.3f %%\n",percentage);
     
   }
   
   /**
    * this method is checking if the arguments that are being passed in are palidrome or not.
    *
    * @param a this variable holds all the args arguments.
    */
   public static boolean isPalindrome(String a){
      a = a.toLowerCase();
      //base condition
      if(a.length() == 0 || a.length() == 1){
         return true;
      }
      if(a.charAt(0) == a.charAt(a.length()-1)){
         return isPalindrome(a.substring(1, a.length()-1));
      }
      return false;
   }
}

