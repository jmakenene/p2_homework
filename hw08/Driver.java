/**
 * Driver to check my code in the wordContainer class.
 *
 * @author	 Jonathan Makenene
 * @version for Program Design 2
 */

import java.util.*;
import java.io.*;

public class Driver{
   public static void main(String[] args)throws IOException{
   
      CodeTimer timer = new CodeTimer();
		timer.start();//starting my timer
		WordContainer wc =  new WordContainer("../data/smallwords.txt");
		wc.checkPalindrome();
		System.out.println("+--------------------------------------------+");
		wc.getResults();
      timer.stop();//stopping my timer
		System.out.println("Time Elapsed    : " + timer);
		System.out.println("+--------------------------------------------+");
   
   }
}