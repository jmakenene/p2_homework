/**
 * Provides LinkedDictionary class that's inherit from the WordList class.
 *
 * @author Jonathan Makenene
 * @version for Program Design 2
 *
 *
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LinkedDictionary extends WordList{
   
   /**
    * the class constructor.
    *
    * @param datafile read from the file
    */   
   public LinkedDictionary(String datafile) throws Exception{
      BufferedReader br;
      br = new BufferedReader(new FileReader(datafile));
      Word w;
      String line;    
      while((line = br.readLine()) != null){
         w = new Word(line);
         insert(w);
      }
      br.close();
   }
   
   /**
    * this method keep count of the anagrams. 
    *
    * @param w the value to be counted  
    * @return the number of anagrams
    */
   public int countAnagrams(Word w){
      WordNode temp;
      temp = head;
      int counter = 0;
      
      while(temp != null){
         if(w.isAnagram(temp.data)){
            counter++;
         }
         temp = temp.next;
      }
      return counter;
   }
   
   /**
    * this method is checking for all the anagrams.
    *
    *
    */
   public void displayBigAnagramFamilies(){
      
      WordNode temp;
      temp = head;
      while(temp != null){
         if(countAnagrams(temp.data) >= 6){
            remove(temp.data);
         }
         showAnagrams(temp);
         
         temp = temp.next;
          
      }
   }
   
   /**
    * this method show the all the anagrams.
    *
    * @param n variable that's being passed in
    */
   public void showAnagrams(WordNode n){
      WordNode temp;
      temp = n;
      while(temp != null){
         if(n.data.isAnagram(temp.data)){
            System.out.println(temp.data);
         }
         temp = temp.next;
      }
   }
}