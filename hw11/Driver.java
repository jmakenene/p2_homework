/**
 * Driver for testing LinkedDictionary code.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

public class Driver
{
   public static void main(String [] args) throws Exception
   {        
            
      CodeTimer timer = new CodeTimer();//initializing my timer class
      timer.start();//timer start
      LinkedDictionary ld = new LinkedDictionary("../data/mywords.txt");
      ld.displayBigAnagramFamilies();
      timer.stop();//timer stop
      System.out.println("timer: "+timer);//prints the timer.
   }
}

