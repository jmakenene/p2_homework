/**
 * A linked list node holding words.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
public class WordNode
{
   Word data;
   WordNode next;
   
}