/**
 * A linked list of Words.
 *
 * @author  Jonathan Makenene
 * @version P2
 *
 */
public class WordList
{
   protected WordNode head;

	/**
	 * A new list has head pointing nowhere.
	 */
   public WordList()
   {
      head= null;
   }


	/**
	 * Displays contents of the list.
	 */
   public void display()
   {
      WordNode p;
      for (p = head; p != null; p = p.next) {
         System.out.println(p.data);
      }
   }



	/**
	 * In an unordered list we can just add to the front.
	 *
	 * @param val The new element to be inserted into the list.
	 */
   public void insert(Word val)
   {
      WordNode p;
      p = new WordNode();
      p.data = val;
      p.next = head;
      head = p;
   }


	/**
	 * Search the list for the value val.
	 *
	 * @param val the value to be searched for
	 * @return reference to the found node (null if not found)
	 */
   public Word search(Word val)
   {
      WordNode p;
      for(p = head; p != null; p = p.next){
         if(p.data == val){
            return p.data;
         }
      }
      return null;
   }


	/**
	 * Find first occurrence of val (if it exists) and remove it from the list.
	 *
	 * @param val the value to be removed
	 */
   public void remove(Word val)
   {
      WordNode temp = head , prev = null;
      
      //remove the head itself.
      if(temp != null && temp.data == val){
         head = temp.next;
         return;
      }
      
      while(temp != null && temp.data != val){
         prev = temp;
         temp = temp.next;
      }
      
      //if there is no element found in the list.
      if(temp == null)
         return;
         
      prev.next = temp.next;
      
   }  
}