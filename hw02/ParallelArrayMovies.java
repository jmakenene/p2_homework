
/**
 * Personal Movie Inventory System.
 *
 * @author  Jonathan Makenene
 * @version For P2
 * 
 * <pre>
 * This is the starter code for the parallel array version of the movie
 * inventory system.
 * </pre>
*/

import java.util.*;
import java.io.*;

public class ParallelArrayMovies {
   //public static final String DATAFILE = "../data/tinymovielist.txt";
   public static final String DATAFILE = "C:/Users/jonat\\p2_homework\\data\\tinymovielist.txt";
   public static final int MAXMOVIES = 10000;
		
   public static void main(String[] args) throws FileNotFoundException {
      int choice; // user's selection from the menu
      String[] titles = new String[MAXMOVIES];
      String[] genres = new String[MAXMOVIES];
      int[] years = new int[MAXMOVIES];
      int numberOfEntries;
   
      numberOfEntries = loadMovies(titles, genres, years);
      System.out.println("Number of entries read from data file: " + numberOfEntries);
   		//displayAll(titles,genres, years, numberOfEntries);
      do {
         choice = getMenuChoice();
         if (choice == 1)
            numberOfEntries = enterMovie(titles, genres, years, numberOfEntries);
         else if (choice == 2)
            numberOfEntries = deleteMovie(titles, genres, years, numberOfEntries);
         else if (choice == 3)
            displayAll(titles, genres, years, numberOfEntries);
         else if (choice == 4)
            searchByGenre(titles, genres, years, numberOfEntries);
         else if (choice == 5)
            searchByTitle(titles, genres, years, numberOfEntries);
         else if (choice == 6)
            searchByYear(titles, genres, years, numberOfEntries);
      } while (choice != 0);
   
      saveMovie(titles, genres, years, numberOfEntries);
   
      System.out.println("\nTHE END");
   }

	/**
	 * Allow user to enter a new movie.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param n      the actual number of movies currently in the array
	 * @return the new movie count
	 */
   public static int enterMovie(String[] titles, String[] genres, int[] years, int n) {
      Scanner kb = new Scanner(System.in);
      System.out.print("Enter movie title : ");
      titles[n] = kb.nextLine();
      System.out.print("Enter movie genre: ");
      genres[n] = kb.nextLine();
      System.out.print("Enter year of movie: ");
      years[n] = kb.nextInt();
      kb.nextLine();
   
      return n + 1;
   }
	
	/**
	 * Load movies from the data file into the arrays
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @return the actual number of movies loaded into the arrays
	 */
   public static int loadMovies(String[] titles, String[] genres, int[] years) throws FileNotFoundException{
   
      Scanner movieFile;
      int num = 0; //using num to keep track of my index 
 
  		movieFile = new Scanner(new FileInputStream(DATAFILE));
  
    		while(movieFile.hasNextLine()){
   		titles[num] = movieFile.nextLine();
    			genres[num] = movieFile.nextLine();
    			years[num] = movieFile.nextInt();
    			movieFile.nextLine();
    
   			num++;
   		}
    		movieFile.close();
    		return num;
   }
	
	/**
	 * Deleting movie from an array.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param n      the actual number of movies currently in the array
	 */
	
   public static int deleteMovie(String[] titles, String[] genres, int[] years, int n) {
   
      Scanner kb = new Scanner(System.in);
      int arrayDel = 0;
      searchByTitle(titles, genres, years, n);
      System.out.print("Enter the number next to the movie you wish to get rid of: ");
      arrayDel = kb.nextInt();
      kb.nextLine();
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
   
      if(arrayDel >= 0 && arrayDel < n){
         for(int i = arrayDel; i < n-1; i++){
            System.out.printf("%-30s %-20s %4d\n", titles[i], genres[i], years[i]);
            titles[i] = titles[i + 1];
            genres[i] = genres[i + 1];
            years[i] = years[i + 1];
         }
      }else{
         return n;
      }
      return n-1;
   }

	/**
	 * Displays all movie information.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param n      the actual number of movies currently in the array
	 */
   public static void displayAll(String[] titles, String[] genres, int[] years, int n) {
      int i;
      System.out.println("------------------------------------------------");
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for (i = 0; i < n; i++)
         System.out.printf("%-30s %-20s %4d\n", titles[i], genres[i], years[i]);
   }

	/**
	 * Displays menu and get's user's selection
	 *
	 * @return the user's menu selection
	 */
   public static int getMenuChoice() {
      Scanner kb = new Scanner(System.in);
      int choice; // user's selection
   
      System.out.println("\n\n");
      System.out.print("------------------------------------\n");
      System.out.print("[1] Add a Movie\n");
      System.out.print("[2] Delete a Movie\n");
      System.out.print("[3] List All Movies\n");
      System.out.print("[4] Search by Genre\n");
      System.out.print("[5] Search by Title\n");
      System.out.print("[6] Search by Year\n");
      System.out.print("---\n");
      System.out.print("[0] Exit Program\n");
      System.out.print("------------------------------------\n");
      do {
         System.out.print("Your choice: ");
         choice = kb.nextInt();
      } while (choice < 0 || choice > 6);
   
      return choice;
   }

	
	/**
	 * Searching movies by entering the genre.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param n      the actual number of movies currently in the array
	 */
	
   public static void searchByGenre(String[] titles, String[] genres, int[] years, int n){
   
      Scanner kb = new Scanner(System.in);
      System.out.print("Search by Genre: ");
      String genre = kb.nextLine();
   
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for(int i = 0; i < n; i++){
         if(genre.equalsIgnoreCase(genres[i])){
            System.out.printf("%-30s %-20s %4d\n", titles[i], genres[i], years[i]);
         }
      }
   }
	
	/**
	 * Searching movie by titles.
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param n      the actual number of movies currently in the array
	 */
   public static void searchByTitle(String[] titles, String[] genres, int[] years, int n) {
      int i;
      Scanner kb = new Scanner(System.in);
      System.out.print("Enter the Title: ");
      String title = kb.nextLine();
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
      for (i = 0; i < n; i++){
         if(titles[i].toLowerCase().contains(title)){
            System.out.print("["+i+"]");
            System.out.printf("%-30s %-20s %4d\n", titles[i], genres[i], years[i]);
         }
      }
   }
	
	/**
	 * Searching movies by year(Release Date)
	 * 
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param Number the actual number of movies currently in the array
	 */

   public static void searchByYear(String[] titles, String[] genres, int[] years, int Number) {
   
      Scanner kb = new Scanner(System.in);
      System.out.print("Search by Year: ");
      int year = kb.nextInt();
      int i;
      System.out.printf("%-30s %-20s %s\n", "TITLE", "GENRE", "YEAR");
   
      for (i = 0; i < Number; i++) {
         if (year == years[i]) {
            System.out.printf("%-30s %-20s %4d\n", titles[i], genres[i], years[i]);
         }
      }
   }
	
	/**
	 * saveMovie method is used to access and print in the file .
	 *
	 * @param titles array of movie titles
	 * @param genres array of movie genres
	 * @param years  array of movie copyright dates
	 * @param n      the actual number of movies currently in the array
	 */
	 
   public static void saveMovie(String[] titles, String[] genres, int[] years, int n) throws FileNotFoundException{
      PrintStream f = new PrintStream(DATAFILE);
      for(int i = 0; i < n; i++){
         f.println(titles[i]);
         f.println(genres[i]);
         f.println(years[i]);
      }
   
      f.close();
   }
}
